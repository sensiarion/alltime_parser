import json
from pprint import pprint
from selenium import webdriver
import requests
import bs4
import os


def watch_parse(url):
    if 'watch' not in url:
        print('Это не часы!')
        return None

    try:
        req = requests.get(url[:-2])
    except OverflowError:
        return None

    page = bs4.BeautifulSoup(req.text, 'html5lib')
    in_stock = page.find('div', attrs={'class': 'product-available'})
    info = {}

    if in_stock is None:
        info['в наличии'] = True
    else:
        info['в наличии'] = False

    info['цена'] = page.find('div', attrs={'class': 'control-price'}).find('li').text[:-1]

    properties = page.find('div', attrs={'class': 'product-accordion'}).find('table').find_all('tr')
    for prop in properties:
        key = prop.th.text

        if prop.span is not None:
            prop.td.span.div.clear()
            info[key] = prop.td.span.text

        else:
            info[key] = prop.td.text

    description = page.find('div', class_='product-accordion').find('div', attrs={'data-id': 1})
    # вырезаем надоедливые подсказки с лишней информацией
    try:
        description.div.find_next_sibling().find_next_sibling().div.extract()
    except AttributeError:
        pass

    try:
        description = description.text

        info['описание'] = ' '.join(description.split())
    except AttributeError:
        pass

    return info


if __name__ == '__main__':
    pass
    url = "https://www.alltime.ru/watch/ingersoll/I00801/237222/"
    no_url = "https://www.alltime.ru/watch/ingersoll/I02501/225930/"

    counter = 0

    dir_name = './watches/'
    file_list = os.listdir(dir_name)

    for file_name in file_list:
        with open(dir_name + file_name, 'r') as file:
            results = []
            for link in file:
                print(link)
                parsed_info = watch_parse(link)
                if parsed_info is not None:
                    results.append(parsed_info)

            with open('./json/'+file_name[:-4] + '.json', 'w') as res_file:
                res_file.write(json.dumps(results))
