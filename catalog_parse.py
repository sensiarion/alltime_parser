import requests
import bs4
import html5lib
import os

"""url = "https://www.alltime.ru/search/?NAME=CITIZEN"


req = requests.get(url)

soup = bs4.BeautifulSoup(req.text, "html5lib")
all_serached = soup.find_all(attrs={'class': "bcc-post"})
print(soup.find(attrs={'class': 'sort-level-2'}))

for elem in all_serached:
    pass
    # print(domen+elem.a['href'])
"""
domen = "https://www.alltime.ru"
url = "https://www.alltime.ru/search/?NAME="
plus = "&PAGEN_1=2"


def find_all_watches(url, brand_name):
    watches_urls = set()

    brand_name = brand_name.strip().replace(' ', '+')
    brand_url = url + brand_name
    req = requests.get(brand_url)

    parser = bs4.BeautifulSoup(req.text, 'html5lib')
    try:
        get_links(parser, watches_urls)
    except IndexError:
        pass

    counter = 2

    while True:
        print(brand_url + plus[:-1] + str(counter))
        req = requests.get(brand_url + plus[:-1].strip().replace(' ', '+') + str(counter))
        parser = bs4.BeautifulSoup(req.text, 'html5lib')

        counter += 1

        try:
            get_links(parser, watches_urls)
        except IndexError:
            break

    if not os.path.exists(f'watches_{brand_name}.txt'):
        with open(f'watches_{brand_name}.txt', 'w') as result_file:
            for watch in watches_urls:
                result_file.write(watch)
                result_file.write('\n')


def get_links(bs4_obj, watches_list):
    all_serached = bs4_obj.find_all(attrs={'class': "bcc-post"})
    start_len = len(watches_list)

    for result in all_serached:
        url = domen + result.a['href']
        watches_list.add(url)

    if len(watches_list) == start_len:
        raise IndexError


if __name__ == '__main__':
    # find_all_watches(url, 'CITIZEN')
    with open('brands.txt', 'r') as brands:
        for brand in brands:
            print(brand)
            find_all_watches(url, brand)
